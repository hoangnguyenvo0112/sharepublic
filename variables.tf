# Copyright (c) HashiCorp, Inc.
# SPDX-License-Identifier: MPL-2.0

variable "region" {
  description = "AWS region"
  type        = string
  default     = "ap-southeast-2"
}

variable "clusterName" {
  description = "AWS EKS Cluster name"
  type        = string
  default     = "vn-prod-mw"
}

variable "vpcName" {
  description = "AWS EKS Cluster vpc"
  type        = string
  default     = "vn-prod-mw-vpc"
}

variable "instanceType" {
  description = "AWS EKS instance type"
  type        = string
  default     = "c5a.xlarge"
}

variable "cidr_vpc" {
  description = "AWS EKS cidr vpc"
  type        = string
  default     = "10.0.0.0/16"
}

variable "private_subnets" {
  description = "AWS vpc private subnet"
  type        = list(string)
  default     = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
}

variable "public_subnets" {
  description = "AWS vpc public subnet"
  type        = list(string)
  default     = ["10.0.4.0/24", "10.0.5.0/24", "10.0.6.0/24"]
}

variable "access_key" {
  type      = string
  sensitive = true
}

variable "secret_key" {
  type      = string
  sensitive = true
}
